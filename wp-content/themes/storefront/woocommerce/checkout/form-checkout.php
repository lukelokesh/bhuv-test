<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

$data_zipcode = WC_Shipping_Zones::get_zones();

foreach ($data_zipcode as $zone_array) {
	//print_r($zone_array['zone_locations']);
	foreach ($zone_array['zone_locations'] as $zip) {
		$zip_code_A[] = $zip->code;
	}
}
?>
<div class="main-wraper-checkout-page">
    <div id="checkout_main_menu" class="checkout-main-menu">

    </div>
	<?php
	do_action( 'woocommerce_before_checkout_form', $checkout );

	// If checkout registration is disabled and not logged in, the user cannot checkout
	if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
		echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );

		return;
	}

	?>

    <form name="checkout" method="post" class="checkout woocommerce-checkout"
          action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
        <div class="main-container-checkout" id="main-container-checkout">
			<?php if ( $checkout->get_checkout_fields() ) : ?>
                <input type="hidden" id="all_zipcode_value" data-zipcodeall='<?php echo json_encode($zip_code_A,true); ?>'>
				<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

                <div class="col2-set" id="customer_details">
                    <div class="checkout-shipping form-section" id="step_2" data-tab-label="Delivery">
                        <div class="col-2">
							<?php do_action( 'woocommerce_checkout_shipping' ); ?>
                        </div>
                    </div>

                    <div class="checkout-billing form-section" id="step_3" data-tab-label="Billing">
                        <p class="form-row form-row-wide copy-shipping-address">
                            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                                <input class="copy-shipping-address-checkobox" id="copyShippingAddress" type="checkbox"
                                       name="copyShippingAddress" value="1"> <span>Same as Delivery Address?</span>
                            </label>
                        </p>
                        <div class="col-1">
							<?php do_action( 'woocommerce_checkout_billing' ); ?>
                        </div>
                    </div>

                </div>

				<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

			<?php endif; ?>
            <h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<?php do_action( 'woocommerce_checkout_order_review' ); ?>



			<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

        </div>

    </form>

	<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
    <div class="form-navigation">
        <button type="button" class="previous btn btn-info pull-left">&lt; Previous</button>
        <button type="button" class="next btn btn-info pull-right">Next &gt;</button>
        <span class="clearfix"></span>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.min.js"></script>
<script>
    jQuery(function ($) {
       //apply custom validation on zip code field
        var all_zipcodes = $("#all_zipcode_value").data("zipcodeall").map(function (item) {
            return parseInt(item)
        });

        Parsley.addValidator('zipCode', {
            requirementType: 'number',
            validateNumber: function (value, requirement) {
                return all_zipcodes.includes(value);
            },
            messages: {
                en: 'This zip code is not available.',
            }
        });

        $('#shipping_postcode').parsley({zipCode: true})

        //get all section and apply them a class to utilize parsley
        var $sections = $('.main-wraper-checkout-page').find('.form-section');
        //generating tab on top
        var list = $('<ul class="checkout-tab-list"></ul>').appendTo('#checkout_main_menu');

        //create main tab link
        for (var i = 0; i < $sections.length; i++) {
            // New <li> elements are created here and added to the <ul> element.
            list.append('<li id="' + i + '" class="">' + $($sections[i]).data('tab-label') + '</li>');
        }

        $('.validate-required').find(':input').parsley({required: true});

        //making shiping address checkbox checked and hidden
        $('#ship-to-different-address-checkbox').prop({'checked': true, 'readonly': 'readonly'}).hide();

        if (!$('#ship-to-different-address-checkbox').is(':checked')) {
            $('.main-container-checkout .checkout-shipping').find(':input').parsley({required: false})
        }

        if (!$('#createaccount').is(':checked')) {
            $('.main-container-checkout .create-account').find(':input').parsley({required: false})
        }

        //function to navigate next or previous section
        function navigateTo(index) {

            $("#checkout_main_menu .checkout-tab-list li").removeClass('change-color');
            $("#checkout_main_menu .checkout-tab-list li").eq(index).addClass('change-color');
            // Mark the current section with the class 'current'
            $sections
                .removeClass('current')
                .eq(index)
                .addClass('current');
            //debugger;
            // Show only the navigation buttons that make sense for the current section:
            $('.form-navigation .previous').toggle(index > 0);
            var atTheEnd = index >= $sections.length - 1;
            $('.form-navigation .next').toggle(!atTheEnd);
            $('.form-navigation [type=submit]').toggle(atTheEnd);

            if (atTheEnd) {
                $('form.checkout_coupon').show();
            }
            else {
                $('form.checkout_coupon').hide();
            }

            //checking user is login or not
            if ($("body").hasClass("logged-in")) {
                if (index == 2) {
                    $('.form-section.woocommerce-checkout-payment').removeClass('current');
                    $('.shop_table').addClass('current');
                }
                else if (index == 3) {
                    //debugger;
                    $('.shop_table').removeClass('current');
                    $('.form-section.woocommerce-checkout-payment').addClass('current');
                }
                else {
                    $('.shop_table').removeClass('current');
                    $('.form-section.woocommerce-checkout-payment').removeClass('current');
                }
            } else {
                if (index == 3) {
                    $('.form-section.woocommerce-checkout-payment').removeClass('current');
                    $('.shop_table').addClass('current');
                }
                else if (index == 4) {
                    //debugger;
                    $('.shop_table').removeClass('current');
                    $('.form-section.woocommerce-checkout-payment').addClass('current');
                }
                else {
                    $('.shop_table').removeClass('current');
                    $('.form-section.woocommerce-checkout-payment').removeClass('current');
                }
            }

        }

        function curIndex() {
            // Return the current index by looking at which section has the class 'current'
            return $sections.index($sections.filter('.current'));
        }

        // Previous button is easy, just go back
        $('.form-navigation .previous').click(function () {
            navigateTo(curIndex() - 1);
        });

        // Next button goes forward iff current block validates
        $('.form-navigation .next').click(function () {
            var $form;
            if ($('form.woocommerce-form-login').length && curIndex() === 0) {
                $form = $('form.woocommerce-form-login');
            } else {
                $form = $('form.woocommerce-checkout');
            }
            $form.parsley().whenValidate({
                group: 'block-' + curIndex()
            }).done(function () {
                navigateTo(curIndex() + 1);
            });
        });

        $('#newUserCheckout').click(function () {
            $('.form-navigation .next').click();
        });

        // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
        $sections.each(function (index, section) {
            $(section).find(':input').attr('data-parsley-group', 'block-' + index);
        });
        navigateTo(0); // Start at the beginning

        //if create account is checked add validation on field else remove
        $("#createaccount").change(function () {
            if (this.checked) {
                $('.main-container-checkout .create-account').find(':input').parsley({required: true})
            }
            else {
                $('.main-container-checkout .create-account').find(':input').parsley({required: false})
            }
        });

        //if create account is checked add validation on field else remove
        $("#copyShippingAddress").change(function () {
            if (this.checked) {
                $('.checkout-shipping.form-section .col-2 .shipping_address').find(':input').each(function () {
                    $tempValue = $(this).val();
                    var fieldsArr = this.id.split(/_(.*)/);
                    $('#billing_' + fieldsArr[1]).val($tempValue);

                    if (this.id == 'shipping_country') {
                        $('#billing_' + fieldsArr[1]).trigger('change');
                    }
                    if (this.id == 'shipping_state') {
                        $('#billing_' + fieldsArr[1]).trigger('change');
                    }
                });

            }
            else {
                $('.checkout-billing.form-section.current .col-1').find(':input').each(function () {
                    $(this).val('');
                });
            }
        });

       //managing tabs click
        $('ul.checkout-tab-list li').click(function (e) {
            var id = this.id;
            if (curIndex() > this.id) {
                navigateTo(this.id);
            }
            if (curIndex() < this.id) {
                //alert(this.id);
                $formTab = $('form.woocommerce-checkout');
                $formTab.parsley().whenValidate({
                    group: 'block-' + curIndex()
                }).done(function () {
                    navigateTo(id);
                    // debugger;

                });
            }

        });
    });

</script>
<style>
    .form-section {
        display: none;
    }

    .form-section.current {
        display: block !important;
    }

    .main-wraper-checkout-page #order_review_heading {
        display: none !important;
    }

    .checkout-main-menu ul {
        margin: 0;
        padding: 0;
    }

    .checkout-main-menu ul li {
        list-style-type: none;
        display: inline;
    }

    .checkout-main-menu li:not(:first-child):before {
        content: " | ";
    }

    .change-color {
        font-weight: bold;
    }

</style>